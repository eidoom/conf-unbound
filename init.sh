#!/usr/bin/env bash

cp_and_bk() {
	local lcl_path="$PWD/$1"
	local sys_path="/$1"
	local backup="$sys_path.backup"

	if [ -f "$backup" ]; then
		echo "$backup already exists!"
		echo "You probably didn't mean to run init.sh just now as it looks like you've already run it!"
		echo "If you're sure, delete $backup and run it again."
		exit 1
	elif [ -f "$sys_path" ]; then
		echo "Making backup $backup"
		mv "$sys_path" "$backup"
	fi

	echo "Copying over $lcl_path to $sys_path"
	cp "$lcl_path" "$sys_path"
	echo
}

echo "Making sure latest version of unbound and install dependencies are installed"
apt update
apt install -y unbound python3
echo

systemctl disable systemd-resolved.service
systemctl stop systemd-resolved.service
systemctl disable unbound-resolvconf.service
systemctl stop unbound-resolvconf.service
echo

cp_and_bk etc/sysctl.d/unbound.conf
sysctl --system
echo

cp_and_bk etc/unbound/unbound.conf.d/main.conf

cp_and_bk etc/unbound/unbound.conf.d/nsd.conf
echo

./update.sh
echo

cp_and_bk etc/unbound/unbound.conf.d/control.conf
unbound-control-setup
echo

systemctl enable unbound.service
systemctl restart unbound.service
