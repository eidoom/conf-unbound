# [conf-unbound](https://gitlab.com/eidoom/conf-unbound)

[Companion post](https://computing-blog.netlify.app/post/new-dns/#configuration-1)

Run `./init.sh` to initialise, `./apply_conf.sh` to (re)apply the configuration, and `./update.sh` to update the blocklist.
Run from root directory of repository only with `root` permissions.
