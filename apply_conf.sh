#!/usr/bin/env bash

cp_v() {
	local lcl_path="$PWD/$1"
	local sys_path="/$1"

	echo "Copying over $lcl_path to $sys_path"
	cp "$lcl_path" "$sys_path"
	echo
}

cp_v etc/sysctl.d/unbound.conf
sysctl --system
echo

cp_v etc/unbound/unbound.conf.d/main.conf

cp_v etc/unbound/unbound.conf.d/control.conf

cp_v etc/unbound/unbound.conf.d/nsd.conf

./update.sh
