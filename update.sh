#!/usr/bin/env bash

cp_fl() {
	local lcl_path="$PWD/$1"
	local sys_path="/$1"

	echo "Copying over $lcl_path to $sys_path"
	cp "$lcl_path" "$sys_path"
	echo
}

./update.py
cp_fl etc/unbound/unbound.conf.d/blocklist.conf

date > last_update_date

echo Reloading Unbound...
systemctl restart unbound.service
